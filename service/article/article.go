package article

import (
	"context"
	"gitlab.com/codeJudge/core"
	"gitlab.com/codeJudge/model"
	"gitlab.com/codeJudge/service"
	"time"
)

type articleService struct {
	ac core.Article
	ct time.Duration
}

func New(core core.Article, timeout time.Duration) service.Article {
	return &articleService{ac: core, ct: timeout}
}

func (a *articleService) Fetch(ctx context.Context, cursor string, num int64) ([]*model.Article, string, error) {
	return nil, "", nil
}
func (a *articleService) GetByID(ctx context.Context, id int64) (*model.Article, error) {
	return nil, nil
}
