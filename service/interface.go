package service

import (
	"context"
	"gitlab.com/codeJudge/model"
)

type Article interface {
	Fetch(ctx context.Context, cursor string, num int64) ([]*model.Article, string, error)
	GetByID(ctx context.Context, id int64) (*model.Article, error)
}
