package codeJudge

import (
	"github.com/spf13/viper"
)

func init() {
	//SetConfigFile explicitly defines the path, name and extension of the config file. Viper will use this and not check any of the config paths.
	viper.SetConfigFile(`config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func main() {
	//make a db connection
}
