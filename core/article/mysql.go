package article

import (
	"database/sql"
	core2 "gitlab.com/codeJudge/core"
	model2 "gitlab.com/codeJudge/model"
	"golang.org/x/net/context"
)

type articleCore struct {
	DB *sql.DB
}

//// New will create an implementation of Article core Interface
func New(db *sql.DB) core2.Article {
	return &articleCore{
		DB: db,
	}
}

func (a *articleCore) Fetch(ctx context.Context, cursor string, num int64) (res []*model2.Article, nextCursor string, err error) {
	return
}

func (a *articleCore) GetByID(ctx context.Context, id int64) (*model2.Article, error) {
	return nil, nil
}
