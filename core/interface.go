package core

import (
	"context"
	model2 "gitlab.com/codeJudge/model"
)

type Article interface {
	Fetch(ctx context.Context, cursor string, num int64) (res []*model2.Article, nextCursor string, err error)
	GetByID(ctx context.Context, id int64) (*model2.Article, error)
}
